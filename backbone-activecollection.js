/**
 * 
 */
(function(root,$,_,Backbone,module){
	'Use Strict';
	module(root,$,_,Backbone);
}(this,jQuery,_,Backbone,function(root,$,_,Backbone){
	'Use Strict';

	
	var activeCollection = null,

		// module namespace manager
		ns = {
			models: '',
			collections: ''
		};

	/**
	 * a url filter model used in criteria filters collection
	 */
	ns.models.filter = Backbone.Model.extend({
		defaults: {
			attr: '',
			operator: '',
			value: '',
			enabled: true
		},

		idAttribute: 'attr'
	});

	/**
	 * a url sort model used in criteria sort collection
	 */
	ns.models.sort = Backbone.Model.extend({
		defaults: {
			attr: '',
			dir: true 		// true => ascending, false => descending
		},

		idAttribute: 'attr'
	});

	/**
	 * collection to hold the filters to be applied to the activecollection's criteria object
	 */
	ns.collections.filters = Backbone.Collection.extend({
		model: ns.models.filter,

		/**
		 * create a new filter model
		 * @param  {string} attr filter name
		 * @param {string} operator operator to use for evaluating the expression
		 * @param  {string|number} value  evaluation value
		 * @return {object}      filter model object
		 */		
		create: function(attr,operator,value){
			return new ns.models.filter({
				attr: attr,
				operator: operator,
				value: value
			});
		},

		/**
		 * sets|adds a new filter rule to criteria
		 * @param {object|array} filter filter object(plain object|filter model) or an array of
		 *                              filter objects
		 * @events: set:filter
		 * 			set:filter:[attr]
		 */
		setFilter: function(filter){

		},

		/**
		 * remove the named filter(s)
		 * @param  {string|array} attr filter(s) to unset
		 * @events: unset:filter
		 * 			unset:filter:[attr]
		 * @return {null}
		 */
		unsetFilter: function(attr){

		},

		/**
		 * resets the named filter(s) to a default value
		 * @param {string|array} attr filter(s) to reset to their defaults
		 * @events: reset:filter
		 * @return {null}
		 */
		resetFilter: function(attr){

		},

		/**
		 * evaluates|tests for validity a given filter value
		 * @param  {string} operator operator to use for evaluating equality of values
		 * @param  {mixed} actual   actual value to test
		 * @param  {mixed} expect   expected value
		 * @return {boolean}
		 */
		evaluate: function(operator,actual,expect){
			if (!this[operator]){
				throw new Error('evaluate(): Non-existent operator ['+operator+']');
			}

			return this[operator](actual,expect);
		},

		fragment: function(){
			var _filters = [];

			this.each(function(filter){
				var _formater = filter.get('operator') + 'Fragment',
					filterString = '';

				if (!this[_formater]){
					throw new Error('fragment(): Non-existent fragment formater "'+_formater+'"');
				}

				filterString = this[_formater](filter.get('attr'),filter.get('value'));

				if (filterString){
					_filters.push(filterString);
				}
			});

			if (!_filters){
				return '';
			}

			// concatenate all filters
			_filters = _filters.join(' and ');

			return '$filter='+_filters;
		},

		/**
		 * formats the value to valid format i.e strings are wrapped in single quotes while
		 * numbers|boolean are left as is
		 * @param  {mixed} val value to format
		 * @return {mixed}     formated value
		 */	
		formatVal: function(val){
			// wrap strings in quotes
			if (!_.isNumber(val) || !_.isBoolean(val)){
				val = "'"+val+"'";
			}

			return val;
		},

		/**
		 * equal operator
		 * @param  {[type]} actual [description]
		 * @param  {[type]} expect [description]
		 * @return {[type]}        [description]
		 */
		_eq: function(actual,expect){

		},

		/**
		 * not equal
		 * @param  {[type]} actual [description]
		 * @param  {[type]} expect [description]
		 * @return {[type]}        [description]
		 */
		_ne: function(actual,expect){

		},

		/**
		 * less than operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_lt: function(actual,expect){

		},

		/**
		 * less than or equal operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_le: function(actual,expect){

		},

		/**
		 * greater than operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_gt: function(actual,expect){

		},

		/**
		 * greater than or equal operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_ge: function(actual,expect){

		},

		/**
		 * between operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_btn: function(actual,expect){

		},

		/**
		 * not between operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_nbtn: function(actual,expect){

		},

		/**
		 * contains operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_has: function(actual,expect){

		},

		/**
		 * not contains operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_nhas: function(actual,expect){

		}

		/**
		 * equal operator
		 * @param  {[type]} actual [description]
		 * @param  {[type]} expect [description]
		 * @return {[type]}        [description]
		 */
		_eqFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' eq ' + val;
		},

		/**
		 * not equal
		 * @param  {[type]} actual [description]
		 * @param  {[type]} expect [description]
		 * @return {[type]}        [description]
		 */
		_neFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' ne ' + val;
		},

		/**
		 * less than operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_ltFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' lt ' + val;
		},

		/**
		 * less than or equal operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_leFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' le ' + val;
		},

		/**
		 * greater than operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_gtFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' gt ' + val;
		},

		/**
		 * greater than or equal operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_geFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' ge ' + val;
		},

		/**
		 * between operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_btnFragment: function( attr,val ){
			val = val.split(/,/);

			return attr + ' btn [' + val[0] + ',' + val[1] + ']';
		},

		/**
		 * not between operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_nbtnFragment: function( attr,val ){
			val = val.split(/,/);

			return attr + ' nbtn [' + val[0] + ',' + val[1] + ']';
		},

		/**
		 * contains operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_hasFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' has ' + val;
		},

		/**
		 * not contains operator
		 * @param  {mixed} actual actual value being tested
		 * @param  {mixed} expect value to test against
		 * @return {boolean}       
		 */
		_nhasFragment: function( attr,val ){
			val = this.formatVal(val);

			return attr + ' has ' + val;
		}


	});

	/**
	 * collection to hold the sort oders to be applied to the activecollection's criteria object
	 */
	ns.collections.sort = Backbone.Collection.extend({
		model: ns.models.sort,

		/**
		 * sets|adds a new sort rule to criteria
		 * @param {object|array} sort sort object(plain object|sort model) or an array of
		 *                              sort objects
		 * @events: set:sort
		 * 			set:sort:[attr]
		 */
		setSort: function(sort){

		},

		/**
		 * remove the named sort(s)
		 * @param  {string|array} attr sort(s) to unset
		 * @events: unset:sort
		 * 			unset:sort:[attr]
		 * @return {null}
		 */
		unsetSort: function(attr){

		},

		/**
		 * resets the named sort(s) to a default value
		 * @param {string|array} attr sort(s) to reset to their defaults
		 * @events: reset:sort
		 * @return {null}
		 */
		resetSort: function(attr){
		},

		/**
		 * creates a new sort model
		 * @param  {string} attr attribute name
		 * @param  {boolean} dir  direction to sort i.e true => ascending, false => descending
		 *                        @default true
		 * @return {object}      sort model
		 */
		create: function(attr,dir){
			dir = _.isUndefined(dir) || dir ? true : false;

			return new ns.models.sort({
				attr: attr,
				dir: dir
			});
		}
	});

	/**
	 * model to hold the activecollection's criteria details
	 */
	ns.models.criteria = Backbone.Model.extend({
		initialize: function(){
			var filters = {},
				sort = {};

			// set up nested event bubbling
			filters = this.get('filters');
			this.listenTo(filters,'set:filter unset:filter reset:filter',this._triggerFilterChange);

			sort = this.get('sort');
			this.listenTo(sort,'set:sort unset:sort reset:sort',this._triggerSortChange);
		},

		defaults: {
			search: null,
			filters: new ns.collections.filters(),
			offset: 0,
			limit: null,
			sort: new ns.collections.sort(),
			page: 1,
			range: [0,'*']
		}

		_triggerFilterChange: function(){
			this.trigger('change:filter');
		},

		_triggerSortChange: function(){
			this.trigger('change:sort');
		}
	});

	/**
	 * pagination page index for the paginator
	 */
	ns.models.page = Backbone.Model.extend({
		defaults: {
			label: '',
			// should be generated with respect to the active collection's criteria model
			href: ''
		}
	});

	/**
	 * pagination pages for the paginator
	 */
	ns.collections.pages = Backbone.Collection.extend({
		model: ns.models.page
	});

	/**
	 * paginator model for indexing the various pagination pages
	 */
	ns.models.paginator = Backbone.Models.extend({
		defaults: {
			current: 1,
			first: 1,
			last: null,
			prev: '',
			next: '',
			pages: 	new ns.collection.pages()		
		}
	});

	activeCollection = {
		/**
		 * create a new filter model| array of filter models
		 * @param  {string|array} attr [description]
		 * @param {string} operator operator to use for evaluating the expression
		 * @param  {string|number} value  evaluation value
		 * @return {object|array}      filter model object| array of filter models
		 */
		createFilter: function(attr,operator,value){
			var _filter =  this.criteria.get('filters');

			if (_.isArray(attr)){
				var filters = [];

				_.each(attr,function(item){
					filters.push(_filter.create(item[0],item[1],item[2]));
				}.bind(this));

				return filters;
			} 

			return _filter.create(attr,operator,value);
		},

		/**
		 * create a new sort model| array of sort models
		 * @param  {string|array} attr [description]
		 * @param  {boolean} dir  direction to sort true => asc, false => desc
		 * @return {object|array}      sort model object| array of sort models
		 */
		createSort: function(attr,dir){
			var _sort =  this.criteria.get('sort');

			if (_.isArray(attr)){
				var sorts = [];

				_.each(attr,function(item){
					sorts.push(_sort.create(item[0],item[1]));
				}.bind(this));

				return sorts;
			} 

			return _sort.create(attr,dir);
		},

		/**
		 * sets a new filter rule for the current activecollection's criteria object
		 * @param {object|array} filter filter object|array of filter objects
		 */
		addFilter: function(filter){

		},

		addSorter: function(sort){

		},

		removeFilter: function(attr){

		},

		resetFilter: function(attr){

		},

		/**
		 * apply the activecollection's criteria to the current models sorting and filtering 
		 * out the models fitting the criteria.
		 */
		applyCriteria: function(){

		},

		url: function(){

		}

	}

}));