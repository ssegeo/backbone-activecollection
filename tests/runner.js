var expect = chai.expect;
mocha.setup('bdd');

$(function(){
    'use strict';
 
    if (window.mochaPhantomJS) {
        mochaPhantomJS.run();
    } else {
        mocha.run();
    }
});