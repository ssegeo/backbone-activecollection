var expect = chai.expect;

mocha.setup('bdd');

// tests
/*{%= specs %}*/


$(function(){
    'use strict';

    /**
     * @todo  consider running tests on demand otherthan having to run them all
     */    
    if (window.mochaPhantomJS) {
        mochaPhantomJS.run();
    } else {
        mocha.run();
    }
});