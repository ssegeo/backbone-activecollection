module.exports = function(grunt){
	'use strict';

	// load plugins
	require('load-grunt-tasks')(grunt);

	grunt.tasksOptions = {
	};


	// project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

	    watch: {
	    	// task1: {
	    	// 	files: 'file|pattern',
	    	// 	tasks: ['tasks to run']
	    	// }
	    },
	    
	    uglify: {
	    	// app
	    	// minify app js
	    	prod: {
	    		options: {
	    			preserveComments: false,
	    		},	    		
	    		files: {
	    			'backbone-activecollection-min.js': ['backbone-activecollection.js']
	    		}
	    	}
	    },

		// generate the mocha runner.js with the current spec files
	    mocha_runner: {
	    	build: {
		    	files: {
		    		'tests/runner.js':'tests/specs/*_spec.js'
		    	},
		    	options: {
		    		runner_template: 'tests/vendors/tpl_runner.js'
		    	}	    		
	    	}
	    },
	});		

	// generate mocha tests runner
	grunt.registerMultiTask('mocha_runner','Generate a Mocha Tests Runner',function(){
		var options = this.options({
				runner_template: 'vendors/tpl_runner.js'
			});

		if (!grunt.file.exists(options.runner_template)){
			grunt.log.error('The runner template file: "'+options.runner_template+'" doesn\'t exist');

			return;
		}

		// obtain the source files
		this.files.forEach(function (f) {
			var specs = [],
				specFiles = grunt.file.expand(f.src),
				destFile = f.dest;

			// loop through all obtained spec files
			specFiles.forEach(function(specFile){
				var spec = grunt.file.read(specFile);

				if (spec){
					specs.push(spec);
				}
			});

			processRunnerTemplate(specs,destFile);
		});

		function processRunnerTemplate(specs,destFile){
			if (!specs.length){
				grunt.log.writeln('There are no tests to run');
			}

			// concatenate the specs
			var allSpecs = specs.join(';\n'),
				delimiter = {
					name: 'lo_comment',
					opener: '/*{%',
					closer: '%}*/'
				},
				templateOptions = {
					data: {
						specs: allSpecs,
						wrapperHead: '',
						wrapperTail: ''
					},
					delimiters: delimiter.name
				},
				template = grunt.file.read(options.runner_template),
				output = '';

			// set custom template delimiters
			grunt.template.addDelimiters(delimiter.name,delimiter.opener,delimiter.closer);
			
			output = grunt.template.process(template,templateOptions);

			grunt.file.write(destFile, output);
		}
	});

	grunt.registerTask('default', ['watch']);
};